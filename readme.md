# Test duty overview

## Requisites
* **[DONE]** The user on the landing page should see a summary list (top 10) of the most expensive appliances.
* **[DONE]** The user on the landing page should see a summary list (top 10) of the most cheap appliances.
* **[DONE]** The user should be able to add appliances to his wishlist
* **[DONE]** The user should be able to remove appliances to his wishlist
* **[DONE]** The user should be able see his wishlist
* **[DONE]** The wishlist interaction should have access control policies (auth user's only)
* **[DONE]** The user should be able to register.
* **[DONE]** The user should be able to log in.
* **[DONE]** The user should be able tu update it's profile data.
* **[DONE]** The user profile edition should have access control policies (auth user's only)
* *[TODO]* The appliances data should come from scraping the URL https://www.appliancesdelivered.ie/search

## Usage

Homestead based app

Clone it

Share it's folder with Homestead

Add a site for the same folder

Point local dns to the site-name you just configured

Enjoy

## Git respository "big picture"

```
*   51e2b87 - Merge branch 'wishlist' into develop (HEAD -> develop, origin/develop)
|\  
| * 45157ac - cart - add items
| * 1b61c44 - cart - remove items
| *   6f5c233 - Merge branch 'develop' into ajax-try
| |\  
| |/  
|/|   
* |   59a8b04 - Merge branch 'profile' into develop
|\ \  
| * | 90def4a - profile edition completed
| * | 555e19b - view - profile
|/ /  
| * 4373f5e - wishlist AJAX interaction
|/  
* cfe40ec - migration - wishlist
* f715b00 - Header adapted to Cart view
* 1afb97f - Added default Laravel default Auth
* 748d1d0 - Appliance - topAffordable + topCheapest, with seeded data.
* def8c85 - model - Appliance - Added model, migration, factory, db seed
* f483446 - view - Appliances - Refactored to component
* 69fdc6e - view - Appliances - First prototype
* 2ce9a1e - Initial commit
```

# Timeline

As in the test duty is stated:

* "it's more to give us an idea of how you approach a problem"
* "and how you structure your application."

I wrote down a commit by commit explanation. Those explanations could be the same I would add to an issue tracker as a description for each feature, or as a comments in a multi commit feature.

## Commit - 2ce9a1e - Initial commit

* Base project created following homestead getting started guidelines.
* It also includes a base template which has became my "helloworld" when triying out new web-dev ideas
    * Base template from [Bootstrap's 3 fixed navbar](http://getbootstrap.com/examples/navbar-fixed-top/)
    * Bower managed web dependencies
    * Wiredep injected bower dependencies
        * In this case using Elixir's way

In this step I learned some basics about the Route->Controller->View on Laravel 5.3

## Commit - 69fdc6e - view - Appliances - First prototype

* First view prototype
* CSS
    * appliance.scss
        * Isolated style for the appliance which is candidate to modularize
        * Semantic classes '.appliance .price.well' to ease readability and understanding of where the selector applies in both the component "appliance price" and it's bootstrap3 component "well".
        * Prefixed '.appliance' class also (modular mindset appart) to avoid selector colision.
    * header.scss, to isolate responsabilities
    * all imported in app.scss
    * Also included custom bootstrap theme just to try out thing's (not best looking, i know).
    * Flagrant copyright infringement with your logo to appeal at your subconscious and try to feel like in-house work as a manner to hack you into be more likely to ignore minor mistakes.
* Hand crafted data for the controller (didn't know about faker by the time).

In this step I learned some basics about CSS management in Laravel 5.3 + SASS

```
app/Http/Controllers/MainController.php  |   32 +++++
public/css/app.css                       |    7 +
public/css/app.css.map                   |    2 +-
public/css/bootstrap-theme-slate.min.css | 1481 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public/img/square1_logo.png              |  Bin 0 -> 2564 bytes
resources/assets/sass/app.scss           |    3 +
resources/assets/sass/appliance.scss     |    3 +
resources/assets/sass/header.scss        |    4 +
resources/views/bootstrap.blade.php      |   92 ++++++++----
routes/web.php                           |    4 +-
10 files changed, 1598 insertions(+), 30 deletions(-)
```

## Commit f483446 - view - Appliances - Refactored to component

(Duplicated code doesn't feel right, right?)

* Master page pattern added (master + home blade's)
* Reusable appliances view (individual and listed)
* First localization try out for landing page.
* Style more SASSy than ever (didn't know about nesting components before)

I don't use to TDD but love the little step's + refactor "smells" apporach.

```
app/Http/Controllers/MainController.php        |  4 ++--
public/css/app.css.map                         |  2 +-
resources/assets/sass/appliance.scss           |  6 ++++--
resources/lang/en/home.php                     |  6 ++++++
resources/views/bootstrap.blade.php            | 67 ++-----------------------------------------------------------------
resources/views/component/appliance.blade.php  | 22 ++++++++++++++++++++++
resources/views/component/appliances.blade.php | 10 ++++++++++
resources/views/page/home.blade.php            |  7 +++++++
resources/views/page/master.blade.php          | 64 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
9 files changed, 118 insertions(+), 70 deletions(-)
```

## Commit def8c85 - model - Appliance - Added model, migration, factory, db seed

* Appliance Migration + Model + Seeding (Factory + Seeder)
    * features as JSON string as quick solution (just started with Eloquent)
* Controller fetching seeded models Eloquent way (raw data, no filtering)

In this step I learned some basics about data models in Laravel

```
app/Appliance.php                                                 |  10 ++++++++++
app/Http/Controllers/MainController.php                           |  38 +++++++++++++++++++++-----------------
database/factories/ApplianceFactory.php                           |  18 ++++++++++++++++++
database/migrations/2016_09_22_170349_create_appliances_table.php |  38 ++++++++++++++++++++++++++++++++++++++
database/seeds/AppliancesTableSeeder.php                          |  16 ++++++++++++++++
database/seeds/DatabaseSeeder.php                                 |   1 +
public/css/app.css.map                                            |   2 +-
resources/assets/sass/appliance.scss                              |   6 ++++--
resources/lang/en/home.php                                        |   6 ++++++
resources/views/bootstrap.blade.php                               | 128 --------------------------------------------------------------------------------------------------------------------------------
resources/views/component/appliance.blade.php                     |  22 ++++++++++++++++++++++
resources/views/component/appliances.blade.php                    |  10 ++++++++++
resources/views/page/home.blade.php                               |   7 +++++++
resources/views/page/master.blade.php                             |  64 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/views/welcome.blade.php                                 |  91 -------------------------------------------------------------------------------------------
15 files changed, 218 insertions(+), 239 deletions(-)
```

## Commit 748d1d0 - Appliance - topAffordable + topCheapest, with seeded data.

* Model enriched with semantic Eloquent data acces methods (candiates to refactor into a DAO or similar in the future)
* Some style sugar (row alignment)

In this step I learned some basics about Eloquent and data fetching.

```
app/Appliance.php                              | 16 +++++++++++++++-
app/Http/Controllers/MainController.php        | 22 +++-------------------
public/css/app.css                             | 12 ++++++++++++
public/css/app.css.map                         |  2 +-
resources/assets/sass/app.scss                 | 13 +++++++++++++
resources/views/component/appliances.blade.php |  2 +-
resources/views/page/home.blade.php            |  5 ++---
7 files changed, 47 insertions(+), 25 deletions(-)
```

## Commit 1afb97f - Added default Laravel default Auth

* Added Laravel default auth (artisan make:auth) + adapting to my views
* Refactored master page to 'layout/app.blade' to follow default auth convention
* Refactored other pages to 'page/\*' to follow view subfolder isolation apporach seen in auth views
* Access control manually tested with route('/cart')

```
app/Http/Controllers/Auth/LoginController.php    |  2 +-
app/Http/Controllers/Auth/RegisterController.php |  2 +-
app/Http/Controllers/HomeController.php          | 31 +++++++++++++++++++++++++++++++
app/Http/Controllers/MainController.php          | 20 --------------------
app/Http/Middleware/RedirectIfAuthenticated.php  |  2 +-
resources/lang/en/cart.php                       |  5 +++++
resources/views/auth/login.blade.php             | 66 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/views/auth/passwords/email.blade.php   | 45 +++++++++++++++++++++++++++++++++++++++++++++
resources/views/auth/passwords/reset.blade.php   | 68 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/views/auth/register.blade.php          | 80 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/views/layouts/app.blade.php            | 87 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/views/page/cart.blade.php              |  5 +++++
resources/views/page/home.blade.php              |  2 +-
resources/views/page/master.blade.php            | 64 ----------------------------------------------------------------
routes/web.php                                   |  6 +++++-
15 files changed, 396 insertions(+), 89 deletions(-)
```

## Commit f715b00 - Header adapted to Cart view

* Updated header to manage home + cart views with "active tab" visual feedback (just now I noticed i missed breadcumbs as another visual feedback).
* Refactored controller common data injection to method 'view_home($target_view, $data)' to ensure cart size data is available in the header without code duplication.

```
app/Http/Controllers/HomeController.php | 25 +++++++++++++++++++++++--
public/css/app.css                      | 12 ++++++++++++
public/css/app.css.map                  |  2 +-
resources/assets/sass/app.scss          | 11 ++++++++++-
resources/views/layouts/app.blade.php   |  9 ++++++++-
5 files changed, 54 insertions(+), 5 deletions(-)
```

## Commit cfe40ec - migration - wishlist

* Wishlist migration as User <-> Appliance many to many relation
* Model User added unidirectional Eloquent relation to Appliance's (wishlist)
* Artisan command to ease User add for manual testing purposes
    * Artisan command versus User seeding tip (avoiding default passwords) thanks to Jorge Torregrosa

```
app/Console/Commands/AddUser.php                                | 51 +++++++++++++++++++++++++++++++++++++++++++++++++++
app/Console/Kernel.php                                          |  4 +++-
app/User.php                                                    |  8 ++++++++
database/migrations/2016_09_23_222933_Added_whishlist_table.php | 38 ++++++++++++++++++++++++++++++++++++++
4 files changed, 100 insertions(+), 1 deletion(-)
```

## Branch ajax-try - Commit 4373f5e - wishlist AJAX interaction

* Appliance component view updated to hold wishlist add/remove action buttons
* Fixed artisan command user-add (missing password encryption)
    * Worked like a charm for "artisan tinker" but found about the bug on actual browser manual testing
* First apporach to AJAX + access control in Laravel
    * Failed miserably the access control part

```
app/Console/Commands/AddUser.php              |  2 +-
app/Http/Controllers/HomeController.php       |  6 +++++-
app/Http/Controllers/WishlistAPI.php          | 14 ++++++++++++++
resources/views/component/appliance.blade.php |  9 +++++++++
resources/views/layouts/app.blade.php         | 11 +++++++++++
routes/api.php                                |  2 ++
6 files changed, 42 insertions(+), 2 deletions(-)
```

## Branch profile - Commit 555e19b - view - profile

Remainder of where this commit belongs
```
* |   59a8b04 - Merge branch 'profile' into develop  <--- Merge branch profile --no-ff to develop
|\ \  
| * | 90def4a - profile edition completed            <--- branch profile
| * | 555e19b - view - profile                       <--- branch profile (HERE WE ARE)
|/ /  
| * 4373f5e - wishlist AJAX interaction              <--- branch ajax-try (AJAX fail)
|/  
* cfe40ec - migration - wishlist                     <--- Common commit
```

Why would I abandon the other branch to start this one ?
1. I was offline (AVE train to Madrid) so I haven't online resources available and had to continue with something I already know how to approach as creating the view/controller for the profile
2. AJAX fail led me to try the "form" approach, and the profile was a good test bed.


* Added reusable profile view (for edit an non edit modes)
    * with error management as seen in Auth views
    * \+ some localization

```
app/Http/Controllers/HomeController.php | 16 ++++++++++++++++
app/Http/Controllers/UserEdit.php       | 35 +++++++++++++++++++++++++++++++++++
resources/lang/en/profile.php           |  6 ++++++
resources/views/layouts/app.blade.php   |  3 +++
resources/views/page/profile.blade.php  | 48 ++++++++++++++++++++++++++++++++++++++++++++++++
routes/api.php                          |  2 ++
routes/web.php                          |  2 ++
7 files changed, 112 insertions(+)
```

## Branch profile - Commit 90def4a - profile edition completed

Remainder of where this commit belongs
```
* |   59a8b04 - Merge branch 'profile' into develop  <--- Merge branch profile --no-ff to develop
|\ \  
| * | 90def4a - profile edition completed            <--- branch profile (HERE WE ARE)
| * | 555e19b - view - profile                       <--- branch profile
|/ /  
| * 4373f5e - wishlist AJAX interaction              <--- branch ajax-try (AJAX fail)
|/  
* cfe40ec - migration - wishlist                     <--- Common commit
```

* Added Debugbar
* Laravel Validator with same rules for name and email as seen in Auth wrapped in a custom validator to allow the User keep it's same email
    * By default the Validator won't accept a email that's already used, but if a user want's to keep it's email that default behaviour should be overrided (as done in the implementation)
* User feedback for error/success
    * Below input
    * \+ Overlay notification (toastr js)
* Style for success notification in isolated profile, semantic & SASSy as it get's
* Some localization in isolated file specific for that view

```
app/Console/Commands/AddUser.php        |   2 +-
app/Http/Controllers/HomeController.php |  26 ++++++++++++++++++++++----
app/Http/Controllers/UserController.php | 119 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
app/Http/Controllers/UserEdit.php       |  35 -----------------------------------
app/User.php                            |   1 +
bower.json                              |  57 +++++++++++++++++++++++++++++----------------------------
composer.json                           |   3 ++-
composer.lock                           | 119 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--
config/app.php                          |   4 +++-
config/debugbar.php                     | 170 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public/css/app.css                      |   3 +++
public/css/app.css.map                  |   2 +-
resources/assets/sass/app.scss          |   1 +
resources/assets/sass/profile.scss      |   5 +++++
resources/lang/en/profile.php           |   2 ++
resources/views/layouts/app.blade.php   |   4 ++++
resources/views/page/profile.blade.php  |  35 +++++++++++++++++++++++++++++++----
routes/api.php                          |   2 --
routes/web.php                          |   6 ++++--
storage/debugbar/.gitignore             |   2 ++
20 files changed, 517 insertions(+), 81 deletions(-)
```

## Commit 59a8b04 Merge --no-ff profile into develop

Merge feature to develop "gitflow"-like

## Branch ajax-try - Commit 6f5c233 - Pull develop changes

Update branch with latest 'develop' integrated features

Remainder of where we are
```
*   51e2b87 - Merge branch 'wishlist' into develop
|\  
| * 45157ac - cart - add items
| * 1b61c44 - cart - remove items                      <--- renamed brach to wishlsit
| *   6f5c233 - Merge branch 'develop' into ajax-try   <--- Pull latest features before continue
| |\  
| |/  
|/|   
* |   59a8b04 - Merge branch 'profile' into develop    <--- Latest features integrated at develop
```

## Branch wishlist - Commit 1b61c44 - cart - remove items

* Added master page js injection
* Cart view injected AJAX implementation of wishlist add/remove
    * with 2 semantic methods reusing the same ajax encapsulation
* Style sugar, fade out on remove from wishlist
    * Bug - As the component is used also in the landing page, you can remove from wishlist there, but the disappear animation doesn't fit (because the appliance should not disapear)

```
app/Http/Controllers/HomeController.php       |  3 ++-
app/Http/Controllers/WishlistAPI.php          |  7 +++++++
resources/views/component/appliance.blade.php |  6 +++---
resources/views/layouts/app.blade.php         |  2 +-
resources/views/page/cart.blade.php           | 43 +++++++++++++++++++++++++++++++++++++++++++
routes/api.php                                |  2 --
routes/web.php                                |  3 +++
7 files changed, 59 insertions(+), 7 deletions(-)
```

## Branch wishlist - Commit 45157ac - cart - add items

* Refactored wishlist JS behaviour
    * to isolated file
    * as self invoked function
    * as module injected via global namespace
    * using php data provided via javascript variable injection on "wishlist-php_inject.blade.php"
        * Seeing the wishlist behaviour as a component which depends on its own JS + some php injections, both are provided as a .blade to include in any view which needs this "wishlist component"
           * examples are page.home and page.cart views
    * JS dynamic data localized via php injection
        * javascript variables "php_inject.wishlist.action_add_text" and "php_inject.wishlist.action_remove_text" hold localized text injected by php.
    * Solution to landing page bug on wishlist remove.
        * Appliance disapears only if injected var "php_inject.wishlist.hide_on_remove = {{ isset($hide_on_remove) && $hide_on_remove ? 'true' : 'false' }};" is true. $hide_on_remove is only set to true in the controller for the cart view, not the landing page.
    * Style sugar with hacky JQuery to change the style of the wishlist action add/remove on interaction
        * The button should be implemented as a template renderable by client and server, as a Mustache template, so instead of JQuery hacking, just passing some data to the template to render it (this way you avoid having to implement the logic twice, one for the server and again for the client)
        * We could also just write in php both buttons with the active displayed and the other 'css display:none', but when changing one for another theres always some weird layout problems, even if you try out some width/heidht animation hacking

```
app/Http/Controllers/HomeController.php                 |  7 ++++---
app/Http/Controllers/WishlistAPI.php                    |  8 ++++++--
gulpfile.js                                             |  3 ++-
public/js/components/wishlist.js                        | 83 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/assets/js/components/wishlist.js              | 83 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
resources/lang/en/cart.php                              |  2 ++
resources/views/component/appliance.blade.php           |  4 ++--
resources/views/component/wishlist-php_inject.blade.php | 11 +++++++++++
resources/views/page/cart.blade.php                     | 41 +----------------------------------------
resources/views/page/home.blade.php                     |  4 ++++
10 files changed, 198 insertions(+), 48 deletions(-)
```

# Thats all

# Well, not really (BONUS)

Ill attach here the first readme, from the first commit, in which i was informally documenting some steps and problems.

Just to have an example on how could be one issue of mine.

In a colaborative environment I would have separated the bug related parts into it's own bug issue + a branch for the hotfix (but i was developing alone and just trying out things).

## (Annex: first commit readme.md) Laravel + bower + wiredep

https://github.com/FabioAntunes/laravel-elixir-wiredep

```javascript
const elixir = require('laravel-elixir');

require('laravel-elixir-vue');
require('laravel-elixir-wiredep'); // <----- Added this

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss')
        .wiredep() // <----- Added this
        .webpack('app.js');
});
```

```shell
/home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/webpack/lib/webpack.js:16
		throw new WebpackOptionsValidationError(webpackOptionsValidationErrors);
		^
WebpackOptionsValidationError: Invalid configuration object. Webpack has been initialised using a configuration object that does not match the API schema.
 - configuration has an unknown property 'babel'. These properties are valid:
   object { amd?, bail?, cache?, context?, devServer?, devtool?, entry, externals?, loader?, module?, name?, dependencies?, node?, output?, plugins?, profile?, recordsInputPath?, recordsOutputPath?, recordsPath?, resolve?, resolveLoader?, stats?, target?, watch?, watchOptions? }
    at webpack (/home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/webpack/lib/webpack.js:16:9)
    at Stream.<anonymous> (/home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/webpack-stream/index.js:131:20)
    at _end (/home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/through/index.js:65:9)
    at Stream.stream.end (/home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/through/index.js:74:5)
    at DestroyableTransform.onend (/home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/readable-stream/lib/_stream_readable.js:523:10)
    at DestroyableTransform.g (events.js:286:16)
    at emitNone (events.js:91:20)
    at DestroyableTransform.emit (events.js:185:7)
    at /home/mesa-mint18/org/laravel/workspace/helloworld/node_modules/readable-stream/lib/_stream_readable.js:965:16
    at _combinedTickCallback (internal/process/next_tick.js:67:7)
```

Error related to webpack bug solved in "2.1.0-beta.22" Version

* as seen in: https://laracasts.com/discuss/channels/elixir/gulp-error-in-fresh-laravel-53

So, install unbugged webpack version.

```shell
$ npm install --save-dev webpack@"2.1.0-beta.22"
```

And don't forget (as I did) to patch up the bootstrap<->bower adding the override on bootstrap files, so wiredep can inject them automatically.

```
"overrides": {
    "bootstrap": {
        "main": [
            "less/bootstrap.less",
            "dist/css/bootstrap.css",
            "dist/js/bootstrap.js",
            "dist/fonts/*"
        ]
    }
}
```

Then exclude the already imported bootstrap at *"resources/assets/sass/app.scss"*

```sass
// Bootstrap
//@import "node_modules/bootstrap-sass/assets/stylesheets/bootstrap";

body {
  padding-top: 70px;
}
```
