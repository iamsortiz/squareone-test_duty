<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/cart', 'HomeController@cart')->middleware('auth');
Route::get('/profile', 'HomeController@profile')->middleware('auth')->name('profile');
Route::get('/profile/edit', 'HomeController@profileEdit')->middleware('auth')->name('profile_edit');

Route::post('/profile/edit', 'UserController@doEdit')->middleware('auth');

Route::post('/user/{user_id}/wishlist', 'WishlistAPI@add')->middleware('auth');
Route::delete('/user/{user_id}/wishlist/{appliance_id}', 'WishlistAPI@remove')->middleware('auth');
