<?php

return [
    'appliances_top10_expensive' => 'Top-notch appliances',
    'appliances_top10_cheapest' => 'Top affordable appliances',
];
