<?php

return [
    'title' => 'Profile',
    'edit_title' => 'Editing your profile',
    'edited_name_success' => 'Name edited succesfully.',
    'edited_email_success' => 'Email edited succesfully.',
];
