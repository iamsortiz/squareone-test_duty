<?php

return [
    'title' => 'Your appliances wishlist.',
    'action_add' => 'Add to ',
    'action_remove' => 'Remove from ',
];
