(function () {

  //////////////////////////////////////////////////////////////////////////////
  ///
  /// PUBLIC METHODS
  ///
  //////////////////////////////////////////////////////////////////////////////
  function wishlistAdd(appliance_id) {
    _ajax(appliance_id, 'POST', php_inject.wishlist.url);
  }

  function wishlistRemove(appliance_id) {
    _ajax(appliance_id, 'DELETE', php_inject.wishlist.url + '/' + appliance_id);
  }

  //////////////////////////////////////////////////////////////////////////////
  ///
  /// PRIVATE METHODS
  ///
  //////////////////////////////////////////////////////////////////////////////
  function _ajax(appliance_id, http_verb, url) {
    var data = {
      appliance_id: appliance_id
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': php_inject.csrf_token
        }
    });
    $.ajax({
        type: http_verb,
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (data) {
          var cart_count = $('.cart-count').html();
          if(http_verb == 'DELETE') {
            // header cart
            cart_count--;
            if(cart_count < 0) cart_count = 0;
            // appliance card
            if(php_inject.wishlist.hide_on_remove) $('.appliance-holder-' + appliance_id).fadeOut();
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').addClass('btn-success');
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').removeClass('btn-warning');
            $('.appliance-holder-' + appliance_id + ' .wishlist-action .text-holder').html(php_inject.wishlist.action_add_text);
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').prop('onclick',null).off('click');
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').attr('onclick','app.wishlist.add(' + appliance_id +')');
          } else {
            // header cart
            cart_count++;
            // appliance card
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').addClass('btn-warning');
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').removeClass('btn-success');
            $('.appliance-holder-' + appliance_id + ' .wishlist-action .text-holder').html(php_inject.wishlist.action_remove_text);
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').prop('onclick',null).off('click');
            $('.appliance-holder-' + appliance_id + ' .wishlist-action').attr('onclick','app.wishlist.remove(' + appliance_id +')');
          }
          $('.cart-count').html(cart_count);
        },
        error: function (data) {
          console.log(data);
        }
    });
  }

  //////////////////////////////////////////////////////////////////////////////
  ///
  /// EXPOSE MODULE
  ///
  //////////////////////////////////////////////////////////////////////////////
  function expose() {
    // global namespace
    window.app = window.app || {};
    window.app.wishlist = window.app.wishlist || {};
    var wishlist = window.app.wishlist;

    // Exposed methods
    wishlist.add = wishlistAdd;
    wishlist.remove = wishlistRemove;
  }
  expose();
})();
