<script>
  var php_inject = php_inject || {};
  php_inject.wishlist = php_inject.wishlist || {};

  php_inject.wishlist.url = '{{ url('/user/' . $user->id . '/wishlist') }}';
  php_inject.wishlist.action_add_text = '{{ trans('cart.action_add') }}';
  php_inject.wishlist.action_remove_text = '{{ trans('cart.action_remove') }}';
  php_inject.wishlist.hide_on_remove = {{ isset($hide_on_remove) && $hide_on_remove ? 'true' : 'false' }};
  php_inject.csrf_token = '{{ csrf_token() }}';
</script>
<script src="/js/components/wishlist.js"></script>
