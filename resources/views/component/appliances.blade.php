<div class="jumbotron">

  <h1>{{ $title }}</h1>

  <div class="row columns-aligned">
    @foreach ($appliances as $appliance)
    @include('component.appliance', [ 'component_appliance' => $appliance])
    @endforeach
  </div>
</div>
