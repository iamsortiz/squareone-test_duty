<div class="col-md-4 appliance-holder-{{ $component_appliance->id }}">
  <div class="appliance panel">
    <div class="panel-heading">
      <!-- Appliance name -->
      <h3 class="panel-title">{{ $component_appliance->name }}</h3>
    </div>
    <div class="panel-body">
      <!-- Action - Add to wishlist -->
      @if (Auth::check())
        @if (array_search($component_appliance->id, array_column($cart, 'id')) === false)
        <button onclick="app.wishlist.add({{ $component_appliance->id }})" class="btn btn-success btn-block wishlist-action wishlist-add"><span class="text-holder">{{ trans('cart.action_add') }}</span> <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></button>
        @else
        <button onclick="app.wishlist.remove({{ $component_appliance->id }})" class="btn btn-warning btn-block wishlist-action wishlist-remove"><span class="text-holder">{{ trans('cart.action_remove') }}</span> <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></button>
        @endif
      @endif

      <!-- Appliance description -->
      <p>{{ $component_appliance->brand }}</p>

      <!-- Appliance features -->
      <ul class="list-group">
        @foreach (json_decode($component_appliance->features) as $feature)
        <li class="list-group-item">{{ $feature }}</li>
        @endforeach
      </ul>

      <!-- Appliance price -->
      <div class="price well well-sm"> €{{ $component_appliance->price }} </div>
    </div>
  </div>
</div>
