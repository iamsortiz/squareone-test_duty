@extends('layouts.app')

@section('content')
  @include('component.appliances', [ 'title' => trans('home.appliances_top10_expensive'), 'appliances' => $appliances_top_expensive])
  @include('component.appliances', [ 'title' => trans('home.appliances_top10_cheapest'), 'appliances' => $appliances_top_affordable])
@endsection

@section('js')
  @include('component.wishlist-php_inject')
@endsection
