@extends('layouts.app')

@section('content')
  @include('component.appliances', [ 'title' => trans('cart.title'), 'appliances' => $cart])
@endsection

@section('js')
  @include('component.wishlist-php_inject', ['hide_on_remove' => true])
@endsection
