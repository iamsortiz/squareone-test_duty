@extends('layouts.app')

@section('content')
<div class="jumbotron">

  @if ($isEditMode)
  <h1>{{ trans('profile.edit_title') }}</h1>
  @else
  <h1>{{ trans('profile.title') }}</h1>
  @endif

  <form class="form-horizontal profile" role="form" method="POST" action="{{ url('/profile/edit') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name">Name</label>
      <input {{ $isEditMode === true ? '' : 'disabled' }} type="text" class="form-control" id="name" name="name" placeholder="" value="{{ $user->name }}">
      @if ($errors->has('name'))
          <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
          </span>
      @endif
      @if (isset($updated) && array_key_exists('name', $updated) && $updated['name'])
      <span class="help-block success">
          <strong>{{ trans('profile.edited_name_success') }}</strong>
      </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email">Email</label>
      <input {{ $isEditMode === true ? '' : 'disabled' }} type="text" class="form-control" id="email" name="email" placeholder="" value="{{ $user->email }}">
      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
      @if (isset($updated) && array_key_exists('email', $updated) && $updated['email'])
      <span class="help-block success">
          <strong>{{ trans('profile.edited_email_success') }}</strong>
      </span>
      @endif
    </div>

    <div class="form-group">
      @if ($isEditMode)
      <div class="pull-right">
        <a href="{{ url('profile') }}" class="btn btn-danger">Cancel</a>
        <button type="submit" class="btn btn-success">Accept changes</button>
      </div>
      @else
      <a href="{{ url('profile/edit' )}}" class="btn btn-warning btn-block">Edit profile</a>
      @endif
    </div>

  </form>

</div> <!-- .jumbotron END -->
@endsection

@section('js')
<script>

  var hasErrors = {{ $hasErrors }};
  var isSomethingUpdated = {{ $isSomethingUpdated }};

  $(document).ready(function () {
    if(hasErrors) {
      toastr.error('Something went wrong. Please review the information provided.');
    } else if(isSomethingUpdated) {
      toastr.success('Profile edited successfully');
    }

  });
</script>
@endsection
