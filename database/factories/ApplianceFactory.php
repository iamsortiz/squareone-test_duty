<?php

$factory->define(App\Appliance::class, function (Faker\Generator $faker) {

    $features = array();
    $features_count = rand(1,6);
    for ($i=0; $i < $features_count; $i++) {
      array_push($features, $faker->realText($maxNbChars = 50, $indexSize = 1));
    }

    return [
        'name' => $faker->name,
        'brand' => $faker->company,
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 9999),
        'features' => json_encode($features),
    ];
});
