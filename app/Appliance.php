<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appliance extends Model
{
    private static $top_count = 10;

    public static function getTopAffordable()
    {
      return Appliance::orderBy('price', 'asc')
               ->take(Appliance::$top_count)
               ->get();
    }

    public static function getTopExpensive()
    {
      return Appliance::orderBy('price', 'desc')
               ->take(Appliance::$top_count)
               ->get();
    }
}
