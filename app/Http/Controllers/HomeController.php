<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Appliance;
use App\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = Auth::user();
      $appliances_top_expensive = Appliance::getTopExpensive();
      $appliances_top_affordable = Appliance::getTopAffordable();

      return $this->view_home('page.home', compact('appliances_top_expensive', 'appliances_top_affordable', 'user'));
    }

    public function cart()
    {
      //Empty cart just to check the user access control implemented in this feature
      $cart = array();
      $user = Auth::user();

      return $this->view_home('page.cart', compact('cart', 'user'));
    }

    public function profile(Request $request)
    {
      // Data retrieval
      $updated = $request->session()->get('updated');
      $user = Auth::user();

      // View mode
      $isEditMode = false;

      // JS inyected data (booleans as strings)
      $hasErrors = 'false';
      $isSomethingUpdated = (isset($updated) && count($updated)) ? 'true' : 'false';

      return $this->view_home('page.profile', compact('user', 'isEditMode', 'updated', 'hasErrors', 'isSomethingUpdated'));
    }

    public function profileEdit(Request $request)
    {
      // Data retrieval
      $errors = $request->session()->get('errors');
      $user = Auth::user();

      // View mode
      $isEditMode = true;

      \Debugbar::info('profileEdit()', $errors);

      // JS inyected data (booleans as strings)
      $hasErrors = (isset($errors) && $errors->count() > 0) ? 'true' : 'false';
      $isSomethingUpdated = 'false';

      return $this->view_home('page.profile', compact('user', 'isEditMode', 'hasErrors', 'isSomethingUpdated'));
    }

    /**
     * Custom view() method to autoinject cart size data in every HomeController page.
     *
     * The header uses a $cart_size value in every page so it's injection it's encapsulated in this method.
     *
     * @param  string $target_view Target view, as in default laravel view() method.
     * @param  array $data        Data to pass to the view, as in default laravel view() method.
     * @return string              HTLM to render, as in default laravel view() method.
     */
    protected function view_home($target_view, $data)
    {
      // Mocked cart size data with Auth check to avoid DB hit on non authenticated users.
      if (Auth::check()) {
        $cart = Auth::user()->wishlist()->get()->all();
        $cart_size = count($cart);

        $data['cart'] = $cart;
        $data['cart_size'] = $cart_size;
      }

      return view($target_view, $data);
    }
}
