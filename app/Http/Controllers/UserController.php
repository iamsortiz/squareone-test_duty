<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

use App\Http\Requests;

use Validator;

class UserController extends Controller
{
    public function doEdit(Request $request)
    {
      $data = $request->input();

      // Custom validation + manual $errors session flashing
      $errors = $this->validate_customized($data);
      $request->session()->flash('errors', $errors);

      if($errors->count() > 0) {
        return redirect()->route('profile_edit');
      } else {
        // No errors, so update
        $updated = $this->user_update($data);

        \Debugbar::info('doEdit - post user_update()', $updated);

        $request->session()->flash('updated', $updated);
        return redirect()->route('profile');
      }
    }

    /**
     * Custom validation to keep the convenience email rule 'unique:users' but fixing a edge case
     *
     * Edge case: When a user modifies it's information but the email remains the same, the 'unique:users' rule fails the validation.
     *
     * The implementation keeps the orginal validation logic but detects/corrects the edge case.
     *
     * @param  array  $data
     * @return MessageBag  $errors (https://laravel.com/api/4.2/Illuminate/Validation/Validator.html#method_errors)
     */
    protected function validate_customized($data)
    {
      $validator = $this->validator($data);
      $errors = $validator->errors();
      $response = $errors;

      if($validator->fails()) {
        $editedEmail = $data['email'];
        $isSameUserEmail = (Auth::user()->email == $editedEmail) ? true : false;

        // If is edge case, remove the error
        if($errors->first('email') && $isSameUserEmail) {
          $error_msg_fixed = $errors->toArray();
          unset($error_msg_fixed['email']);
          $errors = new MessageBag($error_msg_fixed);
          $response = $errors;
        }
      }

      return $errors;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        ]);
    }

    /**
     * Updates the authenticated user's name and email
     *
     * @param  [type] $data [description]
     * //@return [type]       [description]
     */
    public static function user_update($data)
    {
      $user = Auth::user();
      $hasChanged = false;
      $updated = array();

      $attr_name = 'name';
      $new_attr = $data[$attr_name];
      $old_attr = $user->name;
      if(isset($new_attr) && $new_attr != $old_attr) {
          $user->name = $new_attr;
          $hasChanged = true;
          $updated[$attr_name] = true;
          \Debugbar::info("doEdit - in user_update() post update $attr_name", $updated);
      }

      $attr_name = 'email';
      $new_attr = $data[$attr_name];
      $old_attr = $user->email;
      if(isset($new_attr) && $new_attr != $old_attr) {
          $user->email = $new_attr;
          $hasChanged = true;
          $updated[$attr_name] = true;
          \Debugbar::info("doEdit - in user_update() post update $attr_name", $updated);
      }
      \Debugbar::info("doEdit - in user_update() post updates", $updated);

      if($hasChanged) $user->save();

      return $updated;
    }
}
