<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

class WishlistAPI extends Controller
{
    public function add(Request $request) {
      $appliance_id = $request->input('appliance_id');

      $user = Auth::user();
      $user->wishlist()->attach($appliance_id);
      return response('Ok', 200)->header('Content-Type', 'text/plain');
    }

    public function remove(Request $request, $user_id, $appliance_id) {
      $user = Auth::user();
      $user->wishlist()->detach($appliance_id);
      return response('Ok', 200)->header('Content-Type', 'text/plain');
    }
}
