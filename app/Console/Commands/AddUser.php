<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class AddUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'debug:add-user {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add\'s a User with email and password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $password = bcrypt($this->argument('password'));

        $user = new User;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->name = $email;
        $user->save();
    }
}
